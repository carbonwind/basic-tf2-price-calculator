from math import modf

class Price():
    def __init__(self, p_keys, p_refined):
        self.keys = p_keys
        self.refined = p_refined
    def __str__(self):
        return f"Price: {self.keys} keys, {self.refined} refined"

def calculate_price(p_price, p_amount, p_keyvalue = 67.66):
    total_refprice = (p_price.keys * p_keyvalue + p_price.refined) * p_amount

    # For debugging / testing.
    print(f"Unadjusted price (in refined) {total_refprice}")

    keys = total_refprice // p_keyvalue # Keys to pay
    refined = round(total_refprice % p_keyvalue, 2) # Refined to pay
    rounded_scrap = str(round(modf(refined)[0], 2))
    
    if refined == p_keyvalue:
        keys += 1
        refined = 0
    elif len(str(round(modf(refined)[0], 2))) == 3:
        if round(modf(refined)[0], 2) == 0.9:
            refined = int(modf(refined)[1] + 1)
        else:
            scrap_string = "0."+rounded_scrap[2]+rounded_scrap[2]
            refined = modf(refined)[1] + float(scrap_string)
    elif str(round(modf(refined)[0], 2))[2] < str(round(modf(refined)[0], 2))[3]:
        scrap_string = "0." + str(int(rounded_scrap[2])+1) + str(int(rounded_scrap[2])+1)
        refined = modf(refined)[1] + float(scrap_string)
    elif str(round(modf(refined)[0], 2))[2] > str(round(modf(refined)[0], 2))[3]:
        scrap_string = "0."+rounded_scrap[2]+rounded_scrap[2]
        refined = modf(refined)[1] + float(scrap_string)
      
    return Price(int(keys), refined)

def menu():   
    key_value = float(input("Enter the current ref price of a key: "))
    key_price = int(input("Enter item's key price: "))
    ref_price = float(input("Enter item's ref price: "))
    amount = int(input("Enter item amount: "))
    print("---")
    print(calculate_price(Price(key_price, ref_price), amount, key_value), end="\n\n")

while True:
    menu()


